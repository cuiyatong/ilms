jQuery(document).ready(function($){
	//set some variables
	var isAnimating = false,
		firstLoad = false,
		newScaleValue = 1;

	//cache DOM elements
	var dashboard = $('.cd-side-navigation'),
		mainContent = $('.cd-main'),
		loadingBar = $('#cd-loading-bar');

	//select a new section
	dashboard.on('click', 'a', function(event){
		event.preventDefault();
		var target = $(this),
			//detect which section user has chosen
			sectionTarget = target.data('menu');
		if( !target.hasClass('selected') && !isAnimating ) {
			//if user has selected a section different from the one alredy visible - load the new content
			triggerAnimation(sectionTarget, true);
		}

		firstLoad = true;
	});

	//detect the 'popstate' event - e.g. user clicking the back button
  	$(window).on('popstate', function() {
	  	if( firstLoad ) {
		    /*
		    Safari emits a popstate event on page load - check if firstLoad is true before animating
		    if it's false - the page has just been loaded 
		    */
	      	var newPageArray = location.pathname.split('/'),
	        //this is the url of the page to be loaded 
	        newPage = newPageArray[newPageArray.length - 1].replace('.html', '');
	      	if( !isAnimating ) triggerAnimation(newPage, false);
	    }
	    firstLoad = true;
	});

  	//scroll to content if user clicks the .cd-scroll icon
	mainContent.on('click', '.cd-scroll', function(event){
		event.preventDefault();
		var scrollId = $(this.hash);
		$(scrollId).velocity('scroll', { container: $(".cd-section") }, 200);
	});

	//start animation
	function triggerAnimation(newSection, bool) {
		isAnimating =  true;
		newSection = ( newSection == '' ) ? 'index' : newSection;
		
		//update dashboard
		dashboard.find('*[data-menu="'+newSection+'"]').addClass('selected').parent('li').siblings('li').children('.selected').removeClass('selected');
		//trigger loading bar animation
		initializeLoadingBar(newSection);
		//load new content
		loadNewContent(newSection, bool);
	}

	function initializeLoadingBar(section) {
		var	selectedItem =  dashboard.find('.selected'),
			barHeight = selectedItem.outerHeight(),
			barTop = selectedItem.offset().top,
			windowHeight = $(window).height(),
			maxOffset = ( barTop + barHeight/2 > windowHeight/2 ) ? barTop : windowHeight- barTop - barHeight,
			scaleValue = ((2*maxOffset+barHeight)/barHeight).toFixed(3)/1 + 0.001;
		
		//place the loading bar next to the selected dashboard element
		loadingBar.data('scale', scaleValue).css({
		    height: barHeight,
		    top: barTop
		}).attr('class', '').addClass('loading '+section);
	}

	function loadNewContent(newSection, bool) {
		window.location.href = newSection+'.html';
		// setTimeout(function(){
		// 	//animate loading bar
		// 	loadingBarAnimation();
		//
		// 	//create a new section element and insert it into the DOM
		// 	var section = $('<section class="cd-section overflow-hidden '+newSection+'"></section>').appendTo(mainContent);
		// 	//load the new content from the proper html file
		// 	section.load(newSection+'.html .cd-section > *', function(event){
		// 		//finish up the animation and then make the new section visible
		// 		var scaleMax = loadingBar.data('scale');
        //
         //        //将被加载页的JavaScript加载到本页执行
         //        $event = $(event);
		// 		$script = $event.find("script")
         //        section.prev('.visible').removeClass('visible').end().addClass('visible').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
         //            resetAfterAnimation(section);
         //            $script.appendTo('.'+newSection);
         //        });
         //        var url = newSection+'.html';
        //
         //        if(url!=window.location && bool){
         //            //add the new page to the window.history
         //            //if the new page was triggered by a 'popstate' event, don't add it
         //            window.history.pushState({path: url},'',url);
         //            // window.location.reload()
         //        }
		// 		// loadingBar.velocity('stop').velocity({
		// 		// 	scaleY: scaleMax
		// 		// }, 400, function(){
		// 		// 	//add the .visible class to the new section element -> it will cover the old one
		// 		//
         //        //
		// 		// 	//if browser doesn't support transition
		// 		// 	if( $('.no-csstransitions').length > 0 ) {
		// 		// 		resetAfterAnimation(section);
		// 		// 	}
         //         //    var url = newSection+'.html';
         //        //
         //         //    if(url!=window.location && bool){
         //         //        //add the new page to the window.history
         //         //        //if the new page was triggered by a 'popstate' event, don't add it
         //         //        window.history.pushState({path: url},'',url);
         //         //        // window.location.reload()
         //         //    }
         //        //
		// 		// });
		// 	});
        //
		// }, 50);
	}

	function loadingBarAnimation() {
		var scaleMax = loadingBar.data('scale');
		if( newScaleValue + 1 < scaleMax) {
			newScaleValue = newScaleValue + 1;
		} else if ( newScaleValue + 0.5 < scaleMax ) {
			newScaleValue = newScaleValue + 0.5;
		}
		
		loadingBar.velocity({
			scaleY: newScaleValue
		}, 100, loadingBarAnimation);
	}

	function resetAfterAnimation(newSection) {
		//once the new section animation is over, remove the old section and make the new one scrollable
		newSection.removeClass('overflow-hidden').prev('.cd-section').remove();
		isAnimating =  false;
		//reset your loading bar
		resetLoadingBar();
	}

	function resetLoadingBar() {
		loadingBar.removeClass('loading').velocity({
			scaleY: 1
		}, 1);
	}
    showTime()
	if(!window.clockTimer){
		window.clockTimer = setInterval(function(){
			showTime()
		},1000)
	}
	//时间戳格式转换
	function timeSwitch(){
		var date=new Date()
		var result={}
		result.year=date.getFullYear()
		result.month=date.getMonth()+1
		result.date=date.getDate()
		result.weak=date.getDay()
		result.hour=date.getHours()
		result.minute=date.getMinutes()
		result.second=date.getSeconds()
		return result
	}
	//显示时间
	function showTime(){
		var time=timeSwitch()
		var dateStr=''
		var timeStr=''
		var newDateStr=''
		dateStr+=time.year+"年&nbsp;"+time.month+'月&nbsp;'+time.date+"日&nbsp;&nbsp;"
		newDateStr=time.year+'-'+(time.month<10?('0'+time.month):time.month)+'-'+(time.date<10?('0'+time.date):time.date)
		var week=''
		switch(time.weak){
			case 1:
				week='星期一';
				break;
            case 2:
                week='星期二';
                break;
            case 3:
                week='星期三';
                break;
            case 4:
                week='星期四';
                break;
            case 5:
                week='星期五';
                break;
            case 6:
                week='星期六';
                break;
            case 7:
                week='星期日';
                break;
		}
		dateStr+=week
		time.hour<10 ? timeStr=timeStr+'0'+time.hour+":":timeStr=timeStr+time.hour+":"
        time.minute<10 ? timeStr=timeStr+'0'+time.minute+":":timeStr=timeStr+time.minute+":"
        time.second<10 ? timeStr=timeStr+'0'+time.second:timeStr=timeStr+time.second
		$('#dateContent').html(dateStr)
		$('#timeContent').html(timeStr)
		if($('#setting_current_date')){
			$('#_current_date').html('当前日期：'+newDateStr)
			$('#_current_time').html('当前时间：'+timeStr)
		}
	}
});