 $(document).ready(function () {
            function getWeather() {
                $.ajax({
                    url: "http://restapi.amap.com/v3/weather/weatherInfo?key=7174b3ed7c25a28d4992b16eac512bb8&city=520102",
                    dataType: "json",
                    type: "get",
                    async: true,
                    success: function (data) {
                        if (data.lives[0].weather.indexOf("云") != -1) {
                            $(".weather-icon").addClass("wind");
                        };
                        if (data.lives[0].weather.indexOf("阴") != -1) {
                            $(".weather-icon").addClass("wind");
                        };
                        if (data.lives[0].weather.indexOf("雨") != -1) {
                            $(".weather-icon").addClass("cloud");
                        };
                        if (data.lives[0].weather.indexOf("晴") != -1) {
                            $(".weather-icon").addClass("sun");
                        };
                        var humidity = data.lives[0].humidity;
                        $("#wea").text(data.lives[0].weather);
                        $("#temp").text(data.lives[0].temperature + "℃");
                        $("#windredis").text(data.lives[0].winddirection);
                        $("#windpower").text(data.lives[0].windpower);
                        $("#humidity").text(humidity + "%");

                        var date = new Date()
                        $("#_we").text(data.lives[0].weather);
                        $("#_tempr").text(data.lives[0].temperature + "℃");
                        $("#_wind").text(data.lives[0].winddirection+'风'+data.lives[0].windpower+'级');
                        $("#_humidity").text('湿度'+humidity + "%");
                        $("#_date").text(
                            date.getFullYear()+'-'
                            +((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'-'
                            +(date.getDate()>9?date.getDate():'0'+date.getDate())+' '
                            +(date.getHours()>9?date.getHours():'0'+date.getHours())+':'
                            +(date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes()))
                    }
                });
            }
            getWeather();
            setInterval(getWeather, 1000 * 60 * 60);
        });