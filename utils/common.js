/**
 * 获取URL参数
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}

/**
 * 公用ajax请求
 * @url
 * @type 请求的类型
 * @data 传递参数
 * @successCallback 返回成功的回调
 * @async 是否异步执行
 * @errorCallback 返回错误的回调
 * @isHideLoading 是否隐藏loading
 */
function ajaxRequest( url, type, data, successCallback, errorCallback, async) {
    var contentType = "application/json;charset=UTF-8"
    data = data || {}
    data = (type == "post" ? JSON.stringify(data) : data)
    async = (async === false) ? false : true

    $.ajax({
        url: url,
        type: type,
        dataType: 'json',
        contentType: contentType,
        async: async,
        data: data,
        cache: false,
        success: function ( data ) {
            successCallback(data)
        },
        error: function (error ) {
            errorCallback(error)
        },
        complete: function ( ) {
        }
    })
}