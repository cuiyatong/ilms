$(function(){

}())
//获取总览信息
var onlineRatio = 0;
var MyMarhq = '';
var str = '';
var itemObj = []
$('.tbl-body tbody').empty();
$('.tbl-header tbody').empty();

function getPandectInfo() {
    var url = 'https://www.tsdt.work/DSR/api/SysBaseInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url, type, data, function success(res) {
        console.log(res)
        //今日总能耗
        var energy = outputdollars(res.energy.toString())
        console.log($('#_energy'))
        $('#_energy').html(energy+'&nbsp;'+'KwH')
        //系统运行累计时间
        $('#_sysUpTime').html(res.sysUpTime+'&nbsp;'+'天')
        //系统累计亮化时间
        $('#_lightUpTime').html(res.lightUpTime+'&nbsp;'+'小时')
        //单体总数
        $('#_numOfSites').html(res.numOfSites+'&nbsp;'+'个')
        //单体在线数
        $('#_numOfOnlines').html(res.numOfOnlines+'&nbsp;'+'个')
        //单体离线数
        $('#_numOffLines').html((res.numOfSites-res.numOfOnlines)+'&nbsp;'+'个')
        //单体在线率
        onlineRatio = (res.onlineRatio * 100 ).toFixed(2);
        getGauge()
        $('#_onlineRatio').html(res.onlineRatio * 100+'&nbsp;'+'%')
        //系统总能耗
        var totalEnergy = outputdollars(res.totalEnergy.toString())
        $('#_totalEnergy').html(totalEnergy+'&nbsp;'+'kw')
    },function error(error) {
        console.log(error)
    })
}
//格式化金额
function outputdollars(number) {
    if (number.length <= 3)
        return (number == '' ? '0' : number);
    else {
        var mod = number.length % 3;
        var output = (mod == 0 ? '' : (number.substring(0, mod)));
        for (i = 0; i < Math.floor(number.length / 3); i++) {
            if ((mod == 0) && (i == 0))
                output += number.substring(mod + 3 * i, mod + 3 * i + 3);
            else
                output += ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
        }
        return (output);
    }
}

function getControlInfo() {
    //单体基本信息api接口
    var url1 = 'https://www.tsdt.work/DSR/api/ControlOnlyInfo';
    //单体网络数据api接口
    var url2 = 'https://www.tsdt.work/DSR/api/ControlOnlyNetInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url1, type, data, function success(res1) {
        ajaxRequest(url2, type, data, function (res2) {
            res1.length && res1.map(function (item,index) {
                var tmp = res2.filter(function (ite, idx) {
                    return item.id === ite.id
                })
                if(tmp.length>0){
                    item.router = tmp[0].router || '';
                    item.corej = tmp[0].corej || '';
                    item.ecue = tmp[0].ecue || '';
                }
            })
            itemObj = res1;
            clearInterval(MyMarhq);
            packetTable()
        })
    },function error(error) {
        console.log(error)
    })
}

function packetTable() {
    var trCount = 0
    itemObj.forEach(function(item,index,arr){
        str = '<tr>'+
            '<td>'+item.name+'</td>'+
            '<td>'+item.controlType+'</td>'+
            '<td>'+item.networkType+'</td>'
        if(item.router === 'true'){
            str += '<td class="green"></td>'
        }else if (item.route === 'false'){
            str += '<td class="red"></td>'
        }else{
            str += '<td></td>'
        }
        if(item.corej === 'true'){
            str += '<td class="green"></td>'
        }else if(item.corej === 'false'){
            str += '<td class="red"></td>'
        }else{
            str += '<td></td>'
        }
        if(item.ecue === 'true'){
            str += '<td class="green"></td>'
        }else if(item.ecue === 'false'){
            str += '<td class="red"></td>'
        }else{
            str += '<td></td>'
        }
        str += '</tr>'
        $('.tbl-body tbody').append(str);
        $('.tbl-header tbody').append(str);
        trCount++
    })

    if(trCount > 10){
        $('.tbl-body tbody').html($('.tbl-body tbody').html()+$('.tbl-body tbody').html());
        $('.tbl-body').css('top', '0');
        var tblTop = 0;
        var speedhq = 50; // 数值越大越慢
        var outerHeight = $('.tbl-body tbody').find("tr").outerHeight();
        function Marqueehq(){
            if(tblTop <= -outerHeight*itemObj.length){
                tblTop = 0;
            } else {
                tblTop -= 1;
            }
            $('.tbl-body').css('top', tblTop+'px');
        }

        MyMarhq = setInterval(Marqueehq,speedhq);

        // 鼠标移上去取消事件
        $(".tbl-header tbody").hover(function (){
            clearInterval(MyMarhq);
        },function (){
            clearInterval(MyMarhq);
            MyMarhq = setInterval(Marqueehq,speedhq);
        })

    }
}




function getGauge() {

    var myChart = echarts.init(document.getElementById('echart-content'));
    var option = {
        tooltip : {
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                restore : {show: false},
                saveAsImage : {show: false}
            }
        },
        series : [
            {
                name:'单体在线率',
                type:'gauge',
                detail : {
                    formatter:'{value}%',
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontWeight: 'normal',
                        fontSize: 20,
                        color: '#fff'
                    }
                },
                data:[{
                    value: onlineRatio,
                    name: '在线率'
                }],
                title : {
                    show : true,
                    offsetCenter: [0, '-30%'],       // x, y，单位px
                    textStyle: {       // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                        fontSize: 16,
                        fontWeight: 'normal',
                        color: 'auto'
                    }
                },
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        // color: [[0.2, '#86b379'],[0.8, '#68a54a'],[1, '#408829']],
                        color: [[0.2, '#f8f8f8'],[0.8, '#f8f8f8'],[1, '#f8f8f8']],
                        width: 13
                    }
                },
                axisTick: {            // 坐标轴小标记
                    splitNumber: 10,   // 每份split细分多少段
                    length :15,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    length :22,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                pointer : {
                    width : 5
                }
            }
        ]
    };
    myChart.setOption(option);
}
var systemRunTimer=null
/**
 *计算剩余已运行时间和剩余时间
 * 参数格式为19:30
 */
function getSystemRunTime(startTimeStr,endTimeStr){
    var startTimeArr=startTimeStr.split(':')
    var endTimeArr=endTimeStr.split(':')
    var startTime=Number(startTimeArr[0])*60*60+Number(startTimeArr[1])*60
    var endTime=Number(endTimeArr[0])*60*60+Number(endTimeArr[1])*60
    var now = new Date()
    var hourNow=now.getHours()
    if(hourNow<startTimeArr[0]){
        $('#runTime').html('00:00:00')
        $('#leftTime').html('-'+timeToStr(endTime-startTime))
    }else if(hourNow>endTimeArr[0]){
        $('#runTime').html(timeToStr(endTime-startTime))
        $('#leftTime').html('-00:00:00')
        clearInterval(systemRunTimer)
        systemRunTimer=null
    }else{
        var minute=now.getMinutes()
        var seconds=now.getSeconds()
        var nowSeconds=hourNow*60*60+minute*60+seconds
        $('#runTime').html(timeToStr(nowSeconds-startTime))
        $('#leftTime').html('-'+timeToStr(endTime-nowSeconds))
    }
}
function timeToStr(seconds){
    var hour=Math.floor(seconds/(60*60))
    seconds=seconds%(60*60)
    var minute=Math.floor(seconds/60)
    seconds=seconds%60
    if(hour<10){
        hour='0'+hour
    }
    if(minute<10){
        minute='0'+minute
    }
    if(seconds<10){
        seconds='0'+seconds
    }
    return hour+":"+minute+":"+seconds
}


function startSystemRunTimer(){
    if(!systemRunTimer){
        systemRunTimer=setInterval(function () {
            getSystemRunTime('19:10','22:30')
        },1000)
    }
}
clearInterval(MyMarhq);
getPandectInfo()
getControlInfo()
getSystemRunTime('19:10','22:30')
startSystemRunTimer()
