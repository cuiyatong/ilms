var date = new Date();
$('#_today').text(
    date.getFullYear()+'-'
    +((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'-'
    +(date.getDate()>9?date.getDate():'0'+date.getDate()))
//组装静态数据
var baseObj = {}
var resultArr = []

getPandectInfo()
getBaseInfo()
function getPandectInfo() {
    var url = 'https://www.tsdt.work/DSR/api/SysBaseInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url, type, data, function success(res) {
        //系统运行累计时间
        $('#_sysUpTime').html(res.sysUpTime+'天')
        //系统累计亮化时间
        $('#_lightUpTime').html(res.lightUpTime+'小时')
        //单体总数
        $('#_numOfSites').html(res.numOfSites+'个')
        //单体在线数
        $('#_numOfOnlines').html(res.numOfOnlines+'个')
    },function error(error) {
        console.log(error)
    })
}
function getControlInfo() {
    //单体能耗、电压、电流数据api接口
    var url2 = 'https://www.tsdt.work/DSR/api/ControlPowerInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url2, type, data, function (res) {
        // controlArr = res
        res.map(function (ite, idx) {
            if(baseObj[ite.id]){
                resultArr.push(Object.assign(baseObj[ite.id], ite))
            }
        })
        packetTable()
    },function error(error) {
        console.log(error)
    })
}
function getBaseInfo() {
    //单体基本信息api接口
    var url1 = 'https://www.tsdt.work/DSR/api/ControlOnlyInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url1, type, data, function success(res) {
        var baseInfo = {}
        res.map(function (item, index) {
            if(!baseInfo[item.id]){
                baseInfo[item.id] = {}
            }
            baseInfo[item.id] = item
        })
        baseObj = baseInfo

        getControlInfo()
    },function error(error) {
        console.log(error)
    })
}
// 封装表格数据
function packetTable() {
    var totalDayE = 0
    for(var i=0; i<resultArr.length; i++){
        var info = resultArr[i]
        var str=''
        var trLength = (Object.keys(info).length-5)/9
        if(trLength >1){
            var totalE = 0
            for(var k =0; k<trLength; k++){
                if(k === 0){
                    totalE += info['E'].value
                }else {
                    totalE += info['E_'+(k+1)].value
                }
            }
            totalDayE += totalE
            for(var j=0; j< trLength; j++){
                if(j === 0){
                    str='<tr>'+
                        '<td class="report-td1" rowspan="'+trLength+'" style="text-align: center;">'+(i+1)+'</td>'+
                        '<td class="report-td2" rowspan="'+trLength+'">'+info.name+'</td>'+
                        '<td class="report-td3" style="text-align: center;">'+info.controlType+'</td>'+
                        '<td class="report-td4" style="text-align: center;">'+info.networkType+'</td>'+
                        '<td class="report-td5" style="text-align: center;">'+info['E'].value+'</td>'+
                        '<td class="report-td6" rowspan="'+trLength+'" style="text-align: center;">'+parseFloat(totalE.toFixed(2))+'</td>'+
                        '<td class="report-td7" style="text-align: center;"></td>'+
                        '<td class="report-td8" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '<td class="report-td9" style="text-align: center;"></td>'+
                        '<td class="report-td10" style="text-align: center;"></td>'+
                        '<td class="report-td11" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '<td class="report-td12" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '</tr>'
                    $('#reportTableBody').append(str)
                }else{
                    str='<tr>'+
                        // '<td class="report-td1" style="text-align: center;">'+(i+1)+'</td>'+
                        // '<td class="report-td2">'+info.name+'</td>'+
                        '<td class="report-td3" style="text-align: center;">'+info.controlType+'</td>'+
                        '<td class="report-td4" style="text-align: center;">'+info.networkType+'</td>'+
                        '<td class="report-td5" style="text-align: center;">'+info['E_'+(j+1)].value+'</td>'+
                        // '<td class="report-td6" style="text-align: center;"></td>'+
                        '<td class="report-td7" style="text-align: center;"></td>'+
                        // '<td class="report-td8" style="text-align: center;"></td>'+
                        '<td class="report-td9" style="text-align: center;"></td>'+
                        '<td class="report-td10" style="text-align: center;"></td>'+
                        // '<td class="report-td11" style="text-align: center;"></td>'+
                        // '<td class="report-td12" style="text-align: center;"></td>'+
                        '</tr>'
                    $('#reportTableBody').append(str)
                }
            }
        }else{
            totalDayE += info['E'].value
            str='<tr>'+
                '<td class="report-td1" style="text-align: center;">'+(i+1)+'</td>'+
                '<td class="report-td2">'+info.name+'</td>'+
                '<td class="report-td3" style="text-align: center;">'+info.controlType+'</td>'+
                '<td class="report-td4" style="text-align: center;">'+info.networkType+'</td>'+
                '<td class="report-td5" style="text-align: center;">'+info.E.value+'</td>'+
                '<td class="report-td6" style="text-align: center;">'+info.E.value+'</td>'+
                '<td class="report-td7" style="text-align: center;"></td>'+
                '<td class="report-td8" style="text-align: center;"></td>'+
                '<td class="report-td9" style="text-align: center;"></td>'+
                '<td class="report-td10" style="text-align: center;"></td>'+
                '<td class="report-td11" style="text-align: center;"></td>'+
                '<td class="report-td12" style="text-align: center;"></td>'+
                '</tr>'
            $('#reportTableBody').append(str)
        }

    }
    var total1 = '<tr>'+
        '<td rowspan="3" colspan="4" style="text-align: right;padding-right: 10px;width: 680px;">总计</td>'+
        '<td class="report-td5"></td>'+
        '<td class="report-td6" style="text-align: center;background: cadetblue;color: #fff;">日能耗合计</td>'+
        '<td class="report-td7" style="text-align: center;"></td>'+
        '<td class="report-td8" style="text-align: center;background: cadetblue;color: #fff;">累计能耗合计</td>'+
        '<td class="report-td9" style="text-align: center;background: cadetblue;color: #fff;">日发送流量</td>'+
        '<td class="report-td10" style="text-align: center;background: cadetblue;color: #fff;">日接收流量</td>'+
        '<td class="report-td11" style="text-align: center;background: cadetblue;color: #fff;">日累计流量</td>'+
        '<td class="report-td12" style="text-align: center;background: cadetblue;color: #fff;">月累计流量</td>'+
        '</tr>';
    $('#reportTableBody').append(total1)
    var total2 = '<tr>'+
        '<td class="report-td5"></td>'+
        '<td class="report-td6" style="text-align: center;background: cadetblue;color: #fff;">'+parseFloat(totalDayE.toFixed(2))+'</td>'+
        '<td class="report-td7" style="text-align: center;"></td>'+
        '<td class="report-td8" style="text-align: center;background: cadetblue;color: #fff;">789374</td>'+
        '<td class="report-td9" style="text-align: center;background: cadetblue;color: #fff;">23532</td>'+
        '<td class="report-td10" style="text-align: center;background: cadetblue;color: #fff;">876456</td>'+
        '<td class="report-td11" style="text-align: center;background: cadetblue;color: #fff;">92375</td>'+
        '<td class="report-td12" style="text-align: center;background: cadetblue;color: #fff;">89658</td>'+
        '</tr>';
    $('#reportTableBody').append(total2)
    var total3 = '<tr>'+
        '<td class="report-td5" style="background: #2f92e6;color: #fff;">4G日流量</td>'+
        '<td class="report-td6" style="text-align: center;background: #2f92e6;color: #fff;">54342</td>'+
        '<td class="report-td7" style="text-align: center;background: #2f92e6;color: #fff;">4G月流量</td>'+
        '<td class="report-td8" style="text-align: center;background: #2f92e6;color: #fff;">675364</td>'+
        '<td class="report-td9" style="text-align: center;background: #2f92e6;color: #fff;">LAN日流量</td>'+
        '<td class="report-td10" style="text-align: center;background: #2f92e6;color: #fff;">9838432</td>'+
        '<td class="report-td11" style="text-align: center;background: #2f92e6;color: #fff;">LAN月流量</td>'+
        '<td class="report-td12" style="text-align: center;background: #2f92e6;color: #fff;">876364</td>'+
        '</tr>'
    $('#reportTableBody').append(total3)
}

function exportPdf() {
    $('#bottom-info').show()
    var date = new Date()
    $('#_curr_date').text(
        date.getFullYear()+'-'
        +((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'-'
        +(date.getDate()>9?date.getDate():'0'+date.getDate())+' '
        +(date.getHours()>9?date.getHours():'0'+date.getHours())+':'
        +(date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes())+':'
        +(date.getSeconds()>9?date.getSeconds():'0'+date.getSeconds()))
    var target = document.getElementsByClassName("report-page")[0];
    
    //获取节点高度，后面为克隆节点设置高度。
    var height = $(target).height()
    //克隆节点，默认为false，不复制方法属性，为true是全部复制。
    var cloneDom = $(target).clone(true);
    //设置克隆节点的css属性，因为之前的层级为0，我们只需要比被克隆的节点层级低即可。
    cloneDom.css({
        "background-color": "white",
        "position": "absolute",
        "top": "0px",
        "z-index": "-999",
        "height": height
    });
    //将克隆节点动态追加到body后面。
    $("body").append(cloneDom);
    //插件生成base64img图片。
    html2canvas(cloneDom, {
        //Whether to allow cross-origin images to taint the canvas
        allowTaint: true,
        //Whether to test each image if it taints the canvas before drawing them
        taintTest: false,
        onrendered: function(canvas) {
            var contentWidth = canvas.width;
            var contentHeight = canvas.height;
            //一页pdf显示html页面生成的canvas高度;
            var pageHeight = contentWidth / 592.28 * 841.89;
            //未生成pdf的html页面高度
            var leftHeight = contentHeight;
            //页面偏移
            var position = 0;
            //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
            var imgWidth = 595.28;
            var imgHeight = 592.28 / contentWidth * contentHeight;
            var pageData = canvas.toDataURL('image/jpeg', 1.0);
            //注①
            var pdf = new jsPDF('', 'pt', 'a4');
            //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
            //当内容未超过pdf一页显示的范围，无需分页
            if(leftHeight < pageHeight) {
                pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth,imgHeight);
            } else {
                while(leftHeight > 0) {
                    //arg3-->距离左边距;arg4-->距离上边距;arg5-->宽度;arg6-->高度
                    pdf.addImage(pageData, 'JPEG', 0, position,imgWidth, imgHeight)
                    leftHeight -= pageHeight;
                    position -= 841.89;
                    //避免添加空白页
                    if(leftHeight > 0) {
                        //注②
                        pdf.addPage();
                    }
                }
            }
            var pdfName = '系统运行报表.'+date.getFullYear()+'年'+((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'月'+(date.getDate()>9?date.getDate():'0'+date.getDate())+'日.贵州双龙亮丽工程.pdf'

            pdf.save(pdfName);
        }
    });
}
