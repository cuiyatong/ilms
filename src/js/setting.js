var longitude = 106.7141;
var latitude = 26.56822;
//接口生成：https://www.51240.com/api/
// document.getElementById("api_iframe_51240").src = "https://www.51240.com/apiiframe/?api_from=51240&api_url=https://richurimo.51240.com/&api_width=98%&api_backgroundcolor=FFFFFF&api_navigation=no";
var half = ['上午', '下午']
var minutes = [
    '00',
    '01','02','03','04','05','06','07','08','09','10'
    ,'11','12','13','14','15','16','17','18','19','20'
    ,'21','22','23','24','25','26','27','28','29','30'
    ,'31','32','33','34','35','36','37','38','39','40'
    ,'41','42','43','44','45','46','47','48','49','50'
    ,'51','52','53','54','55','56','57','58','59'
]
var hours = [
    '00','01','02','03','04','05','06','07','08','09'
    ,'10','11'
]
var onHourUp,onMinuteUp,onHalfUp,offHourUp,offMinuteUp,offHalfUp
function init(){
    var str=''
    for(var i=0;i<minutes.length;i++){
        str+='<li>'+minutes[i]+'</li>'
    }
    $('.on-minute-list').html(str)
    $('.off-minute-list').html(str)
    settingCancel()
}
init()

function settingSure(){
    localStorage.setItem('onhour',$('#on-hour').text())
    localStorage.setItem('onminute',$('#on-minute').text())
    localStorage.setItem('onhalf',$('#on-half').text())
    localStorage.setItem('offhour',$('#off-hour').text())
    localStorage.setItem('offminute',$('#off-minute').text())
    localStorage.setItem('offhalf',$('#off-half').text())
    $('.setting-toast').css('display','block')
    $('.setting-toast').html('设置成功');
    $('.setting-toast').fadeOut(2000)
    setTimeout(function(){
        $('.setting-toast').css('display','none')
    },2000)


}
function settingCancel(){
    if(localStorage.getItem('onhour')){
        onHourUp=Number(localStorage.getItem('onhour'))
        $('.on-hour-list').css('margin-top',-30*onHourUp+'px')
    }else{
        onHourUp=0
        $('.on-hour-list').animate({marginTop:0})
    }
    if(localStorage.getItem('onminute')){
        onMinuteUp=Number(localStorage.getItem('onminute'))
        $('.on-minute-list').css('margin-top',-30*onMinuteUp+'px')
    }else{
        onMinuteUp=0
        $('.on-minute-list').animate({marginTop:0})
    }
    if(localStorage.getItem('onhalf')){
        onHalfUp=Number(localStorage.getItem('onhalf'))
        $('.on-half-list').css('margin-top',-30*onHalfUp+'px')
    }else{
        onHalfUp=0
        $('.on-half-list').animate({marginTop:0})
    }
    if(localStorage.getItem('offhour')){
        offHourUp=Number(localStorage.getItem('offhour'))
        $('.off-hour-list').css('margin-top',-30*offHourUp+'px')
    }else{
        offHourUp=0
        $('.off-hour-list').animate({marginTop:0})
    }
    if(localStorage.getItem('offminute')){
        offMinuteUp=Number(localStorage.getItem('offminute'))
        $('.off-minute-list').css('margin-top',-30*offMinuteUp+'px')
    }else{
        offMinuteUp=0
        $('.off-minute-list').animate({marginTop:0})
    }
    if(localStorage.getItem('offhalf')){
        offHalfUp=Number(localStorage.getItem('offhalf'))
        $('.off-half-list').css('margin-top',-30*offHalfUp+'px')
    }else{
        offHalfUp=0
        $('.off-half-list').animate({marginTop:0})
    }
    $('#on-hour').html(onHourUp)
    $('#on-minute').html(onMinuteUp)
    $('#on-half').html(onHalfUp)
    $('#off-hour').html(offHourUp)
    $('#off-minute').html(offMinuteUp)
    $('#off-half').html(offHalfUp)
    setUnclicked()
}

//设定不可点击效果
function setUnclicked(){
    if(onHourUp ==0){
        $('#on-hour-up').removeClass('unclicked')
        $('#on-hour-down').addClass('unclicked')
    }else if(onHourUp ==11){
        $('#on-hour-down').removeClass('unclicked')
        $('#on-hour-up').addClass('unclicked')
    }
    if(offHourUp ==0){
        $('#off-hour-up').removeClass('unclicked')
        $('#off-hour-down').addClass('unclicked')
    }else if(onHourUp ==11){
        $('#off-hour-down').removeClass('unclicked')
        $('#off-hour-up').addClass('unclicked')
    }
    if(onMinuteUp ==0){
        $('#on-minute-up').removeClass('unclicked')
        $('#on-minute-down').addClass('unclicked')
    }else if(onMinuteUp ==59){
        $('#on-minute-down').removeClass('unclicked')
        $('#on-minute-up').addClass('unclicked')
    }
    if(offMinuteUp ==0){
        $('#off-minute-up').removeClass('unclicked')
        $('#off-minute-down').addClass('unclicked')
    }else if(offMinuteUp ==59){
        $('#off-minute-down').removeClass('unclicked')
        $('#off-minute-up').addClass('unclicked')
    }
    if(onHalfUp == 0){
        $('#on-half-up').removeClass('unclicked')
        $('#on-half-down').addClass('unclicked')
    }else if(onHalfUp == 1){
        $('#on-half-down').removeClass('unclicked')
        $('#on-half-up').addClass('unclicked')
    }
    if(offHalfUp == 0){
        $('#off-half-up').removeClass('unclicked')
        $('#off-half-down').addClass('unclicked')
    }else if(onHalfUp == 1){
        $('#off-half-down').removeClass('unclicked')
        $('#off-half-up').addClass('unclicked')
    }
}
//

$('#on-hour-up').click(function(e){
    $('#on-hour-down').removeClass('unclicked')
    if(onHourUp<11){
        $('.on-hour-list').animate({marginTop:'-='+30+'px'})
        onHourUp++
        $('#on-hour').html(onHourUp)  
    } 
    if(onHourUp==11){
        $('#on-hour').html(onHourUp) 
        $('#on-hour-up').addClass('unclicked')
    }   
})
$('#on-hour-down').click(function(e){
    $('#on-hour-up').removeClass('unclicked')
    if(onHourUp>0){
        $('.on-hour-list').animate({marginTop:'+='+30+'px'})
        onHourUp--
        $('#on-hour').html(onHourUp)
        
    } 
    if(onHourUp == 0){
        $('#on-hour').html(onHourUp)
        $('#on-hour-down').addClass('unclicked') 
    }   
})
$('#on-minute-up').click(function(e){
    $('#on-minute-down').removeClass('unclicked')
    if(onMinuteUp<59){
        $('.on-minute-list').animate({marginTop:'-='+30+'px'})
        onMinuteUp++
        $('#on-minute').html(onMinuteUp)  
    } 
    if(onMinuteUp==59){
        $('#on-minute').html(onMinuteUp)  
        $('#on-minute-up').addClass('unclicked')
    }   
})
$('#on-minute-down').click(function(e){
    $('#on-minute-up').removeClass('unclicked')
    if(onMinuteUp>0){
        $('.on-minute-list').animate({marginTop:'+='+30+'px'})
        onMinuteUp--
        $('#on-minute').html(onMinuteUp)
        
    } 
    if(onMinuteUp == 0){
        $('#on-minute').html(onMinuteUp)
        $('#on-minute-down').addClass('unclicked') 
    }   
})
$('#on-half-up').click(function(e){
    if(onHalfUp<1){
        $('#on-half-down').removeClass('unclicked')
        $('.on-half-list').animate({marginTop:'-='+30+'px'})
        $('#on-half').html(onHalfUp)
        onHalfUp++
    } 
    if(onHalfUp==1){
        $('#on-half').html(onHalfUp)
        $('#on-half-up').addClass('unclicked')
    }   
})
$('#on-half-down').click(function(e){
    if(onHalfUp>0){
        $('#on-half-up').removeClass('unclicked')
        $('.on-half-list').animate({marginTop:'+='+30+'px'})
        $('#on-half').html(onHalfUp)
        onHalfUp--
        
    } 
    if(onHalfUp == 0){
        $('#on-half').html(onHalfUp)
        $('#on-half-down').addClass('unclicked') 
    }   
})

//

$('#off-hour-up').click(function(e){
    $('#off-hour-down').removeClass('unclicked')
    if(offHourUp<11){
        $('.off-hour-list').animate({marginTop:'-='+30+'px'})
        offHourUp++
        $('#off-hour').html(offHourUp)    
    } 
    if(offHourUp==11){
        $('#off-hour').html(offHourUp) 
        $('#off-hour-up').addClass('unclicked')
    }   
})
$('#off-hour-down').click(function(e){
    $('#off-hour-up').removeClass('unclicked')
    if(offHourUp>0){
        $('.off-hour-list').animate({marginTop:'+='+30+'px'})
        offHourUp--
        $('#off-hour').html(offHourUp)
        
    } 
    if(offHourUp == 0){
        $('#off-hour').html(offHourUp)
        $('#off-hour-down').addClass('unclicked') 
    }   
})
$('#off-minute-up').click(function(e){
    $('#off-minute-down').removeClass('unclicked')
    if(offMinuteUp<59){
        $('.off-minute-list').animate({marginTop:'-='+30+'px'})
        offMinuteUp++
        $('#off-minute').html(offMinuteUp)    
    } 
    if(offMinuteUp==59){
        $('#off-minute').html(offMinuteUp) 
        $('#off-minute-up').addClass('unclicked')
    }   
})
$('#off-minute-down').click(function(e){
    $('#off-minute-up').removeClass('unclicked')
    if(offMinuteUp>0){
        $('.off-minute-list').animate({marginTop:'+='+30+'px'})
        offMinuteUp--
        $('#off-minute').html(offMinuteUp)
        
    } 
    if(offMinuteUp == 0){
        $('#off-minute').html(offMinuteUp)
        $('#off-minute-down').addClass('unclicked') 
    }   
})
$('#off-half-up').click(function(e){
    if(offHalfUp<1){
        $('#off-half-down').removeClass('unclicked')
        $('.off-half-list').animate({marginTop:'-='+30+'px'})
        $('#off-half').html(offHalfUp)
        offHalfUp++
    } 
    if(offHalfUp==1){
        $('#off-half').html(offHalfUp)
        $('#off-half-up').addClass('unclicked')
    }   
})
$('#off-half-down').click(function(e){
    if(offHalfUp>0){
        $('#off-half-up').removeClass('unclicked')
        $('.off-half-list').animate({marginTop:'+='+30+'px'})
        $('#off-half').html(offHalfUp)
        offHalfUp--
        
    } 
    if(offHalfUp == 0){
        $('#off-half').html(offHalfUp)
        $('#off-half-down').addClass('unclicked') 
    }   
})

