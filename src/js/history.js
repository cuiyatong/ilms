//生成日期的js函数
(function (undefined) {
    var _global;
    //工具函数
    //配置合并
    function extend (def,opt,override) {
        for(var k in opt){
            if(opt.hasOwnProperty(k) && (!def.hasOwnProperty(k) || override)){
                def[k] = opt[k]
            }
        }
        return def;
    }
    //日期格式化
    function formartDate (y,m,d,symbol) {
        symbol = symbol || '-';
        m = (m.toString())[1] ? m : '0'+m;
        d = (d.toString())[1] ? d : '0'+d;
        return y+symbol+m+symbol+d
    }

    function Schedule (opt) {
        var def = {},
            opt = extend(def,opt,true),
            curDate = opt.date ? new Date(opt.date) : new Date(),
            year = curDate.getFullYear(),
            month = curDate.getMonth(),
            day = curDate.getDate(),
            currentYear = curDate.getFullYear(),
            currentMonth = curDate.getMonth(),
            currentDay = curDate.getDate(),
            selectedDate = '',
            el = document.querySelector(opt.el) || document.querySelector('body'),
            _this = this;
        var bindEvent = function (){
            el.addEventListener('click',function(e){
                switch (e.target.id) {
                    case 'nextMonth':
                        _this.nextMonthFun();
                        break;
                    case 'nextYear':
                        _this.nextYearFun();
                        break;
                    case 'prevMonth':
                        _this.prevMonthFun();
                        break;
                    case 'prevYear':
                        _this.prevYearFun();
                        break;
                    default:
                        break;
                };
                if(e.target.className.indexOf('currentDate') > -1){
                    opt.clickCb && opt.clickCb(year, month+1, e.target.innerHTML);
                    selectedDate = e.target.title;
                    day = e.target.innerHTML;
                    render();
                }
            },false)
        }
        var init = function () {
            var scheduleHd = '<div class="schedule-hd">'+
                '<div>'+
                '<span class="arrow icon iconfont icon-116leftarrowheads" id="prevYear" ></span>'+
                '<span class="arrow icon iconfont icon-112leftarrowhead" id="prevMonth"></span>'+
                '</div>'+
                '<div class="today">'+formartDate(year,month+1,day,'-')+'</div>'+
                '<div>'+
                '<span class="arrow icon iconfont icon-111arrowheadright" id="nextMonth"></span>'+
                '<span class="arrow icon iconfont icon-115rightarrowheads" id="nextYear"></span>'+
                '</div>'+
                '</div>'
            var scheduleWeek = '<ul class="week-ul ul-box">'+
                '<li>日</li>'+
                '<li>一</li>'+
                '<li>二</li>'+
                '<li>三</li>'+
                '<li>四</li>'+
                '<li>五</li>'+
                '<li>六</li>'+
                '</ul>'
            var scheduleBd = '<ul class="schedule-bd ul-box" ></ul>';
            el.innerHTML = scheduleHd + scheduleWeek + scheduleBd;
            bindEvent();
            render();
        }
        var render = function () {
            var fullDay = new Date(year,month+1,0).getDate(), //当月总天数
                startWeek = new Date(year,month,1).getDay(), //当月第一天是周几
                total = (fullDay+startWeek)%7 == 0 ? (fullDay+startWeek) : fullDay+startWeek+(7-(fullDay+startWeek)%7),//元素总个数
                lastMonthDay = new Date(year,month,0).getDate(), //上月最后一天
                eleTemp = [];
            for(var i = 0; i < total; i++){
                if(i<startWeek){
                    eleTemp.push('<li class="other-month"><span class="dayStyle">'+(lastMonthDay-startWeek+1+i)+'</span></li>')
                }else if(i<(startWeek+fullDay)){
                    var nowDate = formartDate(year,month+1,(i+1-startWeek),'-');
                    var addClass = '';
                    selectedDate == nowDate && (addClass = 'selected-style');
                    formartDate(currentYear,currentMonth+1,currentDay,'-') == nowDate && (addClass = 'today-flag');
                    eleTemp.push('<li class="current-month" ><span title='+nowDate+' class="currentDate dayStyle '+addClass+'">'+(i+1-startWeek)+'</span></li>')
                }else{
                    eleTemp.push('<li class="other-month"><span class="dayStyle">'+(i+1-(startWeek+fullDay))+'</span></li>')
                }
            }
            el.querySelector('.schedule-bd').innerHTML = eleTemp.join('');
            el.querySelector('.today').innerHTML = formartDate(year,month+1,day,'-');
        };
        this.nextMonthFun = function () {
            if(month+1 > 11){
                year += 1;
                month = 0;
            }else{
                month += 1;
            }
            render();
            opt.nextMonthCb && opt.nextMonthCb(year,month+1,day);
        },
            this.nextYearFun = function () {
                year += 1;
                render();
                opt.nextYeayCb && opt.nextYeayCb(year,month+1,day);
            },
            this.prevMonthFun = function () {
                if(month-1 < 0){
                    year -= 1;
                    month = 11;
                }else{
                    month -= 1;
                }
                render();
                opt.prevMonthCb && opt.prevMonthCb(year,month+1,day);
            },
            this.prevYearFun = function () {
                year -= 1;
                render();
                opt.prevYearCb && opt.prevYearCb(year,month+1,day);
            }
        init();
    }
    //将插件暴露给全局对象
    _global = (function(){return this || (0,eval)('this')}());
    if(typeof module !== 'undefined' && module.exports){
        module.exports = Schedule;
    }else if (typeof define === "function" && define.amd){
        define(function () {
            return Schedule;
        })
    }else {
        !('Schedule' in _global) && (_global.Schedule = Schedule);
    }

}());

var mySchedule = new Schedule({
    el: '#schedule-box',
    //date: '2018-9-20',
    clickCb: function (y,m,d) {
        alert('日期：'+y+'-'+m+'-'+d)
    },
    nextMonthCb: function (y,m,d) {
        alert('日期：'+y+'-'+m+'-'+d)
    },
    nextYeayCb: function (y,m,d) {
        alert('日期：'+y+'-'+m+'-'+d)
    },
    prevMonthCb: function (y,m,d) {
        alert('日期：'+y+'-'+m+'-'+d)
    },
    prevYearCb: function (y,m,d) {
        alert('日期：'+y+'-'+m+'-'+d)
    }
});
var date = new Date();
$('#_today').text(
    date.getFullYear()+'-'
    +((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'-'
    +(date.getDate()>9?date.getDate():'0'+date.getDate()))
$('#_curr_date').text(
    date.getFullYear()+'-'
    +((date.getMonth()+1)>9?(date.getMonth()+1):'0'+(date.getMonth()+1))+'-'
    +(date.getDate()>9?date.getDate():'0'+date.getDate())+' '
    +(date.getHours()>9?date.getHours():'0'+date.getHours())+':'
    +(date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes()))

//组装静态数据
var baseObj = {}
var resultArr = []

getPandectInfo()
getBaseInfo()
function getPandectInfo() {
    var url = 'https://www.tsdt.work/DSR/api/SysBaseInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url, type, data, function success(res) {
        //系统运行累计时间
        $('#_sysUpTime').html(res.sysUpTime+'天')
        //系统累计亮化时间
        $('#_lightUpTime').html(res.lightUpTime+'小时')
        //单体总数
        $('#_numOfSites').html(res.numOfSites+'个')
        //单体在线数
        $('#_numOfOnlines').html(res.numOfOnlines+'个')
    },function error(error) {
        console.log(error)
    })
}
function getControlInfo() {
    //单体能耗、电压、电流数据api接口
    var url2 = 'https://www.tsdt.work/DSR/api/ControlPowerInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url2, type, data, function (res) {
        // controlArr = res
        res.map(function (ite, idx) {
            if(baseObj[ite.id]){
                resultArr.push(Object.assign(baseObj[ite.id], ite))
            }
        })
        packetTable()
    },function error(error) {
        console.log(error)
    })
}
function getBaseInfo() {
    //单体基本信息api接口
    var url1 = 'https://www.tsdt.work/DSR/api/ControlOnlyInfo';
    var type = 'GET';
    var data = {};
    ajaxRequest(url1, type, data, function success(res) {
        var baseInfo = {}
        res.map(function (item, index) {
            if(!baseInfo[item.id]){
                baseInfo[item.id] = {}
            }
            baseInfo[item.id] = item
        })
        baseObj = baseInfo

        getControlInfo()
    },function error(error) {
        console.log(error)
    })
}
// 封装表格数据
function packetTable() {
    var trCount = 0
    var totalDayE = 0
    for(var i=0; i<resultArr.length; i++){
        var info = resultArr[i]
        var str=''
        var trLength = (Object.keys(info).length-5)/9
        if(trLength >1){
            var totalE = 0
            for(var k =0; k<trLength; k++){
                if(k === 0){
                    totalE += info['E'].value
                }else {
                    totalE += info['E_'+(k+1)].value
                }
            }
            totalDayE += totalE
            for(var j=0; j< trLength; j++){
                if(j === 0){
                    str='<tr>'+
                        '<td class="history-item1" rowspan="'+trLength+'" style="text-align: center;">'+(i+1)+'</td>'+
                        '<td class="history-item2" rowspan="'+trLength+'">'+info.name+'</td>'+
                        '<td class="history-item3" style="text-align: center;">'+info.controlType+'</td>'+
                        '<td class="history-item4" style="text-align: center;">'+info.networkType+'</td>'+
                        '<td class="history-item5" style="text-align: center;">'+info['E'].value+'</td>'+
                        '<td class="history-item6" rowspan="'+trLength+'" style="text-align: center;">'+parseFloat(totalE.toFixed(2))+'</td>'+
                        '<td class="history-item7" style="text-align: center;"></td>'+
                        '<td class="history-item8" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '<td class="history-item9" style="text-align: center;"></td>'+
                        '<td class="history-item10" style="text-align: center;"></td>'+
                        '<td class="history-item11" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '<td class="history-item12" rowspan="'+trLength+'" style="text-align: center;"></td>'+
                        '</tr>'
                    $('#historyBottomBody').append(str)
                }else{
                    str='<tr>'+
                        // '<td class="history-item1" style="text-align: center;">'+(i+1)+'</td>'+
                        // '<td class="history-item2">'+info.name+'</td>'+
                        '<td class="history-item3" style="text-align: center;">'+info.controlType+'</td>'+
                        '<td class="history-item4" style="text-align: center;">'+info.networkType+'</td>'+
                        '<td class="history-item5" style="text-align: center;">'+info['E_'+(j+1)].value+'</td>'+
                        // '<td class="history-item6" style="text-align: center;"></td>'+
                        '<td class="history-item7" style="text-align: center;"></td>'+
                        // '<td class="history-item8" style="text-align: center;"></td>'+
                        '<td class="history-item9" style="text-align: center;"></td>'+
                        '<td class="history-item10" style="text-align: center;"></td>'+
                        // '<td class="history-item11" style="text-align: center;"></td>'+
                        // '<td class="history-item12" style="text-align: center;"></td>'+
                        '</tr>'
                    $('#historyBottomBody').append(str)
                }
            }
        }else{
            totalDayE += info['E'].value
            str='<tr>'+
                '<td class="history-item1" style="text-align: center;">'+(i+1)+'</td>'+
                '<td class="history-item2">'+info.name+'</td>'+
                '<td class="history-item3" style="text-align: center;">'+info.controlType+'</td>'+
                '<td class="history-item4" style="text-align: center;">'+info.networkType+'</td>'+
                '<td class="history-item5" style="text-align: center;">'+info.E.value+'</td>'+
                '<td class="history-item6" style="text-align: center;">'+info.E.value+'</td>'+
                '<td class="history-item7" style="text-align: center;"></td>'+
                '<td class="history-item8" style="text-align: center;"></td>'+
                '<td class="history-item9" style="text-align: center;"></td>'+
                '<td class="history-item10" style="text-align: center;"></td>'+
                '<td class="history-item11" style="text-align: center;"></td>'+
                '<td class="history-item12" style="text-align: center;"></td>'+
                '</tr>'
            $('#historyBottomBody').append(str)
        }

        trCount++
    }
    var total1 = '<tr>'+
    '<td rowspan="3" colspan="4" style="text-align: right;padding-right: 10px;width: 680px;">总计</td>'+
    '<td class="report-td5"></td>'+
    '<td class="report-td6" style="text-align: center;background: cadetblue;color: #fff;">日能耗合计</td>'+
    '<td class="report-td7" style="text-align: center;"></td>'+
    '<td class="report-td8" style="text-align: center;background: cadetblue;color: #fff;">累计能耗合计</td>'+
    '<td class="report-td9" style="text-align: center;background: cadetblue;color: #fff;">日发送流量</td>'+
    '<td class="report-td10" style="text-align: center;background: cadetblue;color: #fff;">日接收流量</td>'+
    '<td class="report-td11" style="text-align: center;background: cadetblue;color: #fff;">日累计流量</td>'+
    '<td class="report-td12" style="text-align: center;background: cadetblue;color: #fff;">月累计流量</td>'+
    '</tr>';
    $('#historyBottomBody').append(total1)
    trCount++
    var total2 = '<tr>'+
        '<td class="report-td5"></td>'+
        '<td class="report-td6" style="text-align: center;background: cadetblue;color: #fff;">'+parseFloat(totalDayE.toFixed(2))+'</td>'+
        '<td class="report-td7" style="text-align: center;"></td>'+
        '<td class="report-td8" style="text-align: center;background: cadetblue;color: #fff;">789374</td>'+
        '<td class="report-td9" style="text-align: center;background: cadetblue;color: #fff;">23532</td>'+
        '<td class="report-td10" style="text-align: center;background: cadetblue;color: #fff;">876456</td>'+
        '<td class="report-td11" style="text-align: center;background: cadetblue;color: #fff;">92375</td>'+
        '<td class="report-td12" style="text-align: center;background: cadetblue;color: #fff;">89658</td>'+
        '</tr>';
    $('#historyBottomBody').append(total2)
    trCount++
    var total3 = '<tr>'+
        '<td class="report-td5" style="background: #2f92e6;color: #fff;">4G日流量</td>'+
        '<td class="report-td6" style="text-align: center;background: #2f92e6;color: #fff;">54342</td>'+
        '<td class="report-td7" style="text-align: center;background: #2f92e6;color: #fff;">4G月流量</td>'+
        '<td class="report-td8" style="text-align: center;background: #2f92e6;color: #fff;">675364</td>'+
        '<td class="report-td9" style="text-align: center;background: #2f92e6;color: #fff;">LAN日流量</td>'+
        '<td class="report-td10" style="text-align: center;background: #2f92e6;color: #fff;">9838432</td>'+
        '<td class="report-td11" style="text-align: center;background: #2f92e6;color: #fff;">LAN月流量</td>'+
        '<td class="report-td12" style="text-align: center;background: #2f92e6;color: #fff;">876364</td>'+
        '</tr>'
    $('#historyBottomBody').append(total3)
    trCount++
    if(trCount > 10){
        $('#historyBottomBody').html($('#historyBottomBody').html()+$('#historyBottomBody').html());
        $('.history-bottom-table').css('top', '0');
        var tblTop = 0;
        var speedhq = 30; // 数值越大越慢
        var outerHeight = $('#historyBottomBody').find("tr").outerHeight();
        function Marqueehq(){
            if(tblTop <= -outerHeight*trCount++){
                tblTop = 0;
            } else {
                tblTop -= 1;
            }
            $('.history-bottom-table').css('top', tblTop+'px');
        }

        MyMarhq = setInterval(Marqueehq,speedhq);

        // 鼠标移上去取消事件
        $("#historyBottomBody").hover(function (){
            clearInterval(MyMarhq);
        },function (){
            clearInterval(MyMarhq);
            MyMarhq = setInterval(Marqueehq,speedhq);
        })

    }
}
function nextPage(){
    var firstTop=$('.first-page').offset().top
    var secondTop=$('.second-page').offset().top
    var marginTop=secondTop-firstTop
    $("#historyPage").animate({marginTop:-marginTop+'px'})
}
function prevPage(){
    var firstTop=$('.first-page').offset().top
    var secondTop=$('.second-page').offset().top
    var marginTop=secondTop-firstTop
    $("#historyPage").animate({marginTop:'+='+marginTop+'px'})
}
// consumeData()
// exportPdf()
function exportPdf() {
        window.location.href = 'report.html'
    }
