$(function () {
    var itemArr = []
    var currentTypeIndex = 0
    var currentGroupIndex = 0
    //组装静态数据
    var typeArr = [
        {
            id: 0,
            name: '商业建筑'
        },
        {
            id: 1,
            name: '企事业单位'
        },
        {
            id: 2,
            name: '景观与桥梁'
        },
        {
            id: 3,
            name: '高层住宅'
        }
    ]
    var dataArr = [
        {
            type: 0,
            group: [
                {
                    groupName: '太升国际大厦',
                    unit: [
                        {
                            id: 'GY_SL_LA08_1',
                            name: 'A区A1#楼'
                        },
                        {
                            id: 'GY_SL_LA08_2',
                            name: 'A区A2#楼'
                        },
                        {
                            id: 'GY_SL_LA08_3',
                            name: 'A区A3#楼'
                        },
                        {
                            id: 'GY_SL_LA08_4',
                            name: 'A区B1#楼'
                        },
                        {
                            id: 'GY_SL_LA08_5',
                            name: 'A区B2#楼'
                        }
                    ],
                    img: 'common/img/W3/W3-tsgj.png'
                },
                {
                    groupName: '多彩贵州文化创意园',
                    unit: [
                        {
                            id: 'GY_SL_DCWCY123',
                            name: '研发中心1#/2#/3#'
                        },
                        {
                            id: 'GY_SL_DCWCY_LD',
                            name: '展示中心'
                        },
                        {
                            id: 'GY_SL_DCWCY_YYZX',
                            name: '研发会议中心2#'
                        },
                        {
                            id: 'GY_SL_DCWCY_456',
                            name: '研发中心4#/5#/6#'
                        },
                        {
                            id: 'GY_SL_DCWCY_YFZX',
                            name: '研发会议中心1#'
                        }
                    ],
                    img: 'common/img/W3/W3-wchy.png'
                },
                {
                    groupName: '砂之船奥特莱斯',
                    unit: [
                        {
                            id: 'GY_SL_LB03_1',
                            name: '1#楼'
                        },
                        {
                            id: 'GY_SL_LB03_2',
                            name: '2#楼'
                        },
                        {
                            id: 'GY_SL_LB03_3',
                            name: '3#楼'
                        },
                        {
                            id: 'GY_SL_LB03_4',
                            name: '4#楼'
                        }
                    ],
                    img: 'common/img/W3/W3-shzhch.png'
                },
                {
                    groupName: '双龙数据工场',
                    unit: [
                        {
                            id: 'GY_SL_LA04',
                            name: '主楼'
                        }
                    ],
                    img: 'common/img/W3/W3-sjgc.png'
                },
                {
                    groupName: '中国文化大数据产业大楼',
                    unit: [
                        {
                            id: 'GY_SL_LC02',
                            name: '主楼'
                        }
                    ],
                    img: 'common/img/W3/W3-CCDI.png'
                }
            ]
        },
        {
            type: 1,
            group: [
                {
                    groupName: '贵阳市委党校',
                    unit: [
                        {
                            id: 'GY_SL_LA09_1',
                            name: '党校主楼'
                        },
                        {
                            id: 'GY_SL_LA09_2',
                            name: '党校宿舍'
                        },
                        {
                            id: 'GY_SL_LA09_3',
                            name: '党校餐厅'
                        },
                        {
                            id: 'GY_SL_LA09_4',
                            name: '党校体育馆'
                        }

                    ],
                    img: 'common/img/W3/W3-swdx.png'
                },
                {
                    groupName: '贵阳学院',
                    unit: [
                        {
                            id: 'GY_SL_LA03_1',
                            name: '音乐学院'
                        },
                        {
                            id: 'GY_SL_LA03_2',
                            name: '博雅楼'
                        }

                    ],
                    img: 'common/img/W3/W3-gyxy.png'
                },
                {
                    groupName: '贵阳神奇学院',
                    unit: [
                        {
                            id: 'GY_SL_LA01',
                            name: '神奇学院'
                        }
                    ],
                    img: 'common/img/W3/W3-sqxy.png'
                },
                {
                    groupName: '武警医院',
                    unit: [
                        {
                            id: 'GY_SL_LA02',
                            name: '武警医院'
                        }
                    ],
                    img: 'common/img/W3/W3-wjxq.png'
                },
                {
                    groupName: '老干妈',
                    unit: [
                        {
                            id: 'GY_SL_LB01',
                            name: '老干妈'
                        }
                    ],
                    img: 'common/img/W3/W3-lgm.png'
                }
            ]
        },
        {
            type: 2,
            group: [
                {
                    groupName: '山体景观',
                    unit: [
                        {
                            id: 'GY_SL_LA13',
                            name: '东北山区域'
                        },
                        {
                            id: 'GY_SL_LA14',
                            name: '东南山区域'
                        },
                        {
                            id: 'GY_SL_LA11',
                            name: '北山区域'
                        }, {
                            id: 'GY_SL_LA12',
                            name: '南山区域'
                        }
                    ],
                    img: 'common/img/W3/W3-jgst.png'
                },
                {
                    groupName: '桥梁景观',
                    unit: [
                        {
                            id: 'GY_SL_LA17',
                            name: '龙腾路桥'
                        },
                        {
                            id: 'GY_SL_LA15',
                            name: '兴业路桥'
                        },
                        {
                            id: 'GY_SL_LA16',
                            name: '龙水路桥'
                        }
                    ],
                    img: 'common/img/W3/W3-jgst.png'
                }
            ]
        },
        {
            type: 3,
            group: [
                {
                    groupName: '经典天成住宅小区',
                    unit: [
                        {
                            id: 'GY_SL_LB02_1',
                            name: '经典天成2#'
                        },
                        {
                            id: 'GY_SL_LB02_2',
                            name: '经典天成12#'
                        }
                    ],
                    img: 'common/img/W3/W3-jdtc.png'
                },
                {
                    groupName: '庭宜住宅小区',
                    unit: [
                        {
                            id: 'GY_SL_LA06_1',
                            name: '庭宜小区(1号楼)'
                        },
                        {
                            id: 'GY_SL_LA06_2',
                            name: '庭宜小区(2号楼)'
                        },
                        {
                            id: 'GY_SL_LA06_3',
                            name: '庭宜小区(3号楼)'
                        },
                        {
                            id: 'GY_SL_LA06_4',
                            name: '庭宜小区(4号楼)'
                        },
                        {
                            id: 'GY_SL_LA06_5',
                            name: '庭宜小区(5号楼)'
                        },
                        {
                            id: 'GY_SL_LA06_6',
                            name: '庭宜小区(6号楼)'
                        }
                    ],
                    img: 'common/img/W3/W3-tyxq.png'
                },
                {
                    groupName: '兴业住宅小区',
                    unit: [
                        {
                            id: 'GY_SL_LA07_1',
                            name: '兴业小区(1号楼)'
                        },
                        {
                            id: 'GY_SL_LA07_2',
                            name: '兴业小区(2号楼)'
                        },
                        {
                            id: 'GY_SL_LA07_3',
                            name: '兴业小区(3号楼)'
                        },
                        {
                            id: 'GY_SL_LA07_4',
                            name: '兴业小区(4   号楼)'
                        },

                    ],
                    img: 'common/img/W3/W3-xyxq.png'
                },
                {
                    groupName: '林园住宅小区',
                    unit: [
                        {
                            id: 'GY_SL_LA05_1',
                            name: '林园小区(1号楼)'
                        },
                        {
                            id: 'GY_SL_LA05_2',
                            name: '林园小区(2号楼)'
                        },
                        {
                            id: 'GY_SL_LA05_3',
                            name: '林园小区(3号楼)'
                        }

                    ],
                    img: 'common/img/W3/W3-lyxq.png'
                }
            ]
        }
    ]

    //切换顶部type
    function setTypeActive(index) {
        $('#consumeType').find('.consume-type-button').removeClass('active').eq(index).addClass('active')
    }

    $('#consumeType').on('click', '.consume-type-button', function () {
        var index = $(this).attr('data-index')
        currentTypeIndex = index
        setTypeActive(index)
        renderGroup(index)
        $('#info_title').text(dataArr[currentTypeIndex].group[0].groupName+'能耗数据')
        //获取静态组装数据中的图片地址
        $('#group_image').attr('src',dataArr[currentTypeIndex].group[0].img)
        getDataByGroup(currentTypeIndex, 0)
    })

    //切换顶部tab
    function setTabActive(index) {
        $('#consumeTab').find('.consume-tab').removeClass('active').eq(index).addClass('active')
    }

    $('#consumeTab').on('click', '.consume-tab', function () {
        var index = $(this).attr('data-index')
        currentGroupIndex = index
        setTabActive(index)
        $('#info_title').text(dataArr[currentTypeIndex].group[currentGroupIndex].groupName+'能耗数据')
        //获取静态组装数据中的图片地址
        $('#group_image').attr('src',dataArr[currentTypeIndex].group[currentGroupIndex].img)
        getDataByGroup(currentTypeIndex,currentGroupIndex)
    })

    function renderType() {
        var typeWrap = $('#consumeType');
        var str = '';
        typeArr.map(function (item, index) {
            if(index === 0) {
                str = '<button class="consume-type-button active" data-index="'+index+'">'+item.name+'</button>'
            }else{
                str = '<button class="consume-type-button" data-index="'+index+'">'+item.name+'</button>'
            }
            typeWrap.append(str)
        })
        renderGroup(0)
        $('#info_title').text(dataArr[0].group[0].groupName+'能耗数据')
        //获取静态组装数据中的图片地址
        $('#group_image').attr('src',dataArr[0].group[0].img)
        getDataByGroup(0,0)
    }

    function renderGroup(index) {
        $('#consumeTab').html('')
        var groupWrap = $('#consumeTab');
        var str = '';
        dataArr[index].group.map(function (item, index) {
            if(index === 0) {
                str = '<div class="consume-tab active" data-index="'+index+'">'+item.groupName+'</div>'
            }else{
                str = '<div class="consume-tab" data-index="'+index+'">'+item.groupName+'</div>'
            }
            groupWrap.append(str)
        })
    }
    var voltageChartData = {}
    var voltageCahrtXias = []
    var electricityChartData = {}
    var powerChartData = {}
    var energyChartData = {}
    function getDataByGroup(typeIndex, groupIndex) {
        voltageChartData = {}
        electricityChartData = {}
        voltageCahrtXias = []
        energyChartData = {}
        powerChartData = {}
        dataArr[typeIndex].group[groupIndex].unit.map(function (item, index) {
            itemArr.map(function (ite, idx) {
                if(item.id === ite.id){
                    for(var i = 0;i< parseInt(Object.keys(ite).length / 9); i++){
                        if(parseInt(Object.keys(ite).length / 9)>1){
                            voltageCahrtXias.push(item.name+'_'+(i+1))
                        }else{
                            voltageCahrtXias.push(item.name)
                        }
                        if(!voltageChartData['Ua'] || !voltageChartData['Ua'].length){
                            voltageChartData['Ua'] = []
                        }
                        if(!voltageChartData['Ub'] || !voltageChartData['Ub'].length){
                            voltageChartData['Ub'] = []
                        }
                        if(!voltageChartData['Uc'] || !voltageChartData['Uc'].length){
                            voltageChartData['Uc'] = []
                        }
                        if(!electricityChartData['Ia'] || !electricityChartData['Ia'].length){
                            electricityChartData['Ia'] = []
                        }
                        if(!electricityChartData['Ib'] || !electricityChartData['Ib'].length){
                            electricityChartData['Ib'] = []
                        }
                        if(!electricityChartData['Ic'] || !electricityChartData['Ic'].length){
                            electricityChartData['Ic'] = []
                        }
                        if(!powerChartData['P'] || !powerChartData['P'].length){
                            powerChartData['P'] = []
                        }
                        if(!energyChartData['E'] || !energyChartData['E'].length){
                            energyChartData['E'] = []
                        }
                        if(i === 0){
                            voltageChartData['Ua'].push(ite.Ua.value)
                            voltageChartData['Ub'].push(ite.Ub.value)
                            voltageChartData['Uc'].push(ite.Uc.value)
                            electricityChartData['Ia'].push(ite.Ia.value)
                            electricityChartData['Ib'].push(ite.Ib.value)
                            electricityChartData['Ic'].push(ite.Ic.value)
                            powerChartData['P'].push(ite.P.value)
                            energyChartData['E'].push(ite.E.value)
                        }else{
                            voltageChartData['Ua'].push(ite['Ua_'+(i+1)].value)
                            voltageChartData['Ub'].push(ite['Ub_'+(i+1)].value)
                            voltageChartData['Uc'].push(ite['Uc_'+(i+1)].value)
                            electricityChartData['Ia'].push(ite['Ia_'+(i+1)].value)
                            electricityChartData['Ib'].push(ite['Ib_'+(i+1)].value)
                            electricityChartData['Ic'].push(ite['Ic_'+(i+1)].value)
                            powerChartData['P'].push(ite['P_'+(i+1)].value)
                            energyChartData['E'].push(ite['E_'+(i+1)].value)
                        }

                    }
                }
            })
        })
        //绘制电压柱状图
        getVoltageChart()
        //绘制电流柱状图
        getElectricityChart()
        //绘制功率图
        getPowerChart()
        //绘制能耗图
        getEnergyChart()
    }

    function getControlInfo() {
        //单体能耗、电压、电流数据api接口
        var url2 = 'https://www.tsdt.work/DSR/api/ControlPowerInfo';
        var type = 'GET';
        var data = {};
        ajaxRequest(url2, type, data, function (res) {
            itemArr = res
            renderType()
        },function error(error) {
            console.log(error)
        })
    }

    function getVoltageChart() {
        var myChart = echarts.init(document.getElementById('echart-voltage'));
        var app = {};
        option = null;
        var posList = [
            'left', 'right', 'top', 'bottom',
            'inside',
            'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
            'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
        ];

        app.configParameters = {
            rotate: {
                min: -90,
                max: 90
            },
            align: {
                options: {
                    left: 'left',
                    center: 'center',
                    right: 'right'
                }
            },
            verticalAlign: {
                options: {
                    top: 'top',
                    middle: 'middle',
                    bottom: 'bottom'
                }
            },
            position: {
                options: echarts.util.reduce(posList, function (map, pos) {
                    map[pos] = pos;
                    return map;
                }, {})
            },
            distance: {
                min: 0,
                max: 100
            }
        };

        app.config = {
            rotate: 90,
            align: 'left',
            verticalAlign: 'middle',
            position: 'insideBottom',
            distance: 15
        };

        var labelOption = {
            normal: {
                show: false,
                position: app.config.position,
                distance: app.config.distance,
                align: app.config.align,
                verticalAlign: app.config.verticalAlign,
                rotate: app.config.rotate,
                formatter: '{c}  {name|{a}}',
                fontSize: 16,
                rich: {
                    name: {
                        color: '#fff',
                        textBorderColor: 'transparent'
                    }
                }
            }
        };

        option = {
            color: ['#FF3333', '#FFFF33', '#66CC33'],
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            toolbox: {
                show: false,
                orient: 'vertical',
                left: 'right',
                top: 'center',
                feature: {
                    mark: {show: false},
                    dataView: {show: false, readOnly: false},
                    magicType: {show: false, type: ['line', 'bar', 'stack', 'tiled']},
                    restore: {show: false},
                    saveAsImage: {show: false}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    axisTick: {show: false},
                    data: voltageCahrtXias,
                    nameTextStyle: {fontSize: 18},
                    axisLine:{show:true,lineStyle: {color: '#fff',fontSize: 18}}
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    nameTextStyle: {fontSize: 18},
                    max: 280,
                    min: 180,
                    axisLine:{show:true,lineStyle: {color: '#fff',fontSize: 18}}
                }
            ],
            series: [
                {
                    name: 'A相电压',
                    type: 'bar',
                    barGap: 0,
                    label: labelOption,
                    data: voltageChartData['Ua']
                },
                {
                    name: 'B相电压',
                    type: 'bar',
                    label: labelOption,
                    data: voltageChartData['Ub']
                },
                {
                    name: 'C相电压',
                    type: 'bar',
                    label: labelOption,
                    data: voltageChartData['Uc']
                }
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }

    function getElectricityChart() {
        var myChart = echarts.init(document.getElementById('echart-electricity'));
        var app = {};
        option = null;
        var posList = [
            'left', 'right', 'top', 'bottom',
            'inside',
            'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
            'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
        ];

        app.configParameters = {
            rotate: {
                min: -90,
                max: 90
            },
            align: {
                options: {
                    left: 'left',
                    center: 'center',
                    right: 'right'
                }
            },
            verticalAlign: {
                options: {
                    top: 'top',
                    middle: 'middle',
                    bottom: 'bottom'
                }
            },
            position: {
                options: echarts.util.reduce(posList, function (map, pos) {
                    map[pos] = pos;
                    return map;
                }, {})
            },
            distance: {
                min: 0,
                max: 100
            }
        };

        app.config = {
            rotate: 90,
            align: 'left',
            verticalAlign: 'middle',
            position: 'insideBottom',
            distance: 15
        };

        var labelOption = {
            normal: {
                show: false,
                position: app.config.position,
                distance: app.config.distance,
                align: app.config.align,
                verticalAlign: app.config.verticalAlign,
                rotate: app.config.rotate,
                formatter: '{c}  {name|{a}}',
                fontSize: 16,
                rich: {
                    name: {
                        color: '#fff',
                        textBorderColor: 'transparent'
                    }
                }
            }
        };

        option = {
            color: ['#FF3333', '#FFFF33', '#66CC33'],
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            toolbox: {
                show: false,
                orient: 'vertical',
                left: 'right',
                top: 'center',
                feature: {
                    mark: {show: false},
                    dataView: {show: false, readOnly: false},
                    magicType: {show: false, type: ['line', 'bar', 'stack', 'tiled']},
                    restore: {show: false},
                    saveAsImage: {show: false}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    axisTick: {show: false},
                    data: voltageCahrtXias,
                    axisLine:{show:true,lineStyle: {color: '#fff'}},
                    nameTextStyle: {color: '#fff'}
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min: 0,
                    max: 5,
                    axisLine:{show:true,lineStyle: {color: '#fff'}},
                    nameTextStyle: {color: '#fff'}
                }
            ],
            series: [
                {
                    name: 'A相电流',
                    type: 'bar',
                    barGap: 0,
                    label: labelOption,
                    data: electricityChartData['Ia']
                },
                {
                    name: 'B相电流',
                    type: 'bar',
                    label: labelOption,
                    data: electricityChartData['Ib']
                },
                {
                    name: 'C相电流',
                    type: 'bar',
                    label: labelOption,
                    data: electricityChartData['Ic']
                }
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }

    function getPowerChart() {
        var myChart = echarts.init(document.getElementById('echart-power'));
        var app = {};
        option = null;
        app.title = '功率-kw';

        option = {
            title: {
                text: '功率-kw',
                textStyle: {
                    color: '#fff',
                    fontSize: 20
                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLine:{show:true,lineStyle: {color: '#fff'}},
                nameTextStyle: {color: '#fff'},
                boundaryGap: [0, 0.01]
            },
            yAxis: {
                type: 'category',
                axisLine:{show:true,lineStyle: {color: '#fff'}},
                data: []
            },
            series: [
                {
                    name: '功率值',
                    type: 'bar',
                    data: powerChartData['P']
                }
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }
    function getEnergyChart() {
        var myChart = echarts.init(document.getElementById('echart-energy'));
        var app = {};
        option = null;
        app.title = '能耗-kwH';

        option = {
            title: {
                text: '能耗-kwH',
                textStyle: {
                    color: '#fff',
                    fontSize: 20
                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'value',
                axisLine:{show:true,lineStyle: {color: '#fff'}},
                nameTextStyle: {color: '#fff'},
                boundaryGap: [0, 0.01]
            },
            yAxis: {
                type: 'category',
                axisLine:{show:true,lineStyle: {color: '#fff'}},
                data: []
            },
            series: [
                {
                    name: '能耗值',
                    type: 'bar',
                    data: energyChartData['E']
                }
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }

    function init() {
        getControlInfo()
    }
    init()
}())