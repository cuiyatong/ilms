var category = []
var equipState= {}
var loopState ={}
var selectedCategory=[]
var selectedEquip={}
var selectedLoop={}
var getDataFlag=false
var selectedGroupName=''
var selectedMonomerName=''
var selectedPictureName=''
// 获取数据信息
function getNetworkControlInfo() {
    //类别接口
    // var url1 = 'https://www.tsdt.work/DSR/api/ControlOnlyInfo';
    //设备和网络状态信息接口
    var url2 = 'https://www.tsdt.work/DSR/api/ControlOnlyNetInfo';
    // 回路信息接口
    var url3 = 'https://www.tsdt.work/DSR/api/ControlCircleInfo'
    var type = 'GET';
    var data = {};
    $.when($.ajax(url2),$.ajax(url3)).done(function(res2,res3){
        equipState = JSON.parse(res2[0])
        loopState = JSON.parse(res3[0])
        getDataFlag=true
    })
}
getNetworkControlInfo()
handleData()
if(getDataFlag){
    selectedCategoryFunction(0)
}else{
    var getDataTimer=setInterval(function(){
        if(getDataFlag){
            selectedCategoryFunction(0)
            clearInterval(getDataTimer)
        }
    },1000)
}



// 组装数据 组装成4大类
function handleData(){
    category=[
        {
            name:'商业建筑',
            arr:[{
                name:'太升国际大厦',
                arr:[
                    {id:'GY_SL_LA08_1',name:'A区-A1#',imgName: 'W2-tsgj'},
                    {id:'GY_SL_LA08_2',name:'A区-A2#',imgName: 'W2-tsgj'},
                    {id:'GY_SL_LA08_3',name:'A区-A3#',imgName: 'W2-tsgj'},
                    {id:'',name:'',imgName: 'W2-tsgj'},
                    {id:'GY_SL_LA08_4',name:'B区-B1#',imgName: 'W2-tsgj'},
                    {id:'GY_SL_LA08_5',name:'B区-B2#',imgName: 'W2-tsgj'}
                ]
            },{
                name:'多彩贵州文化创意园',
                arr:[
                    {id:'GY_SL_DCWCY123',name:'研发中心1#/2#/3#',imgName: 'W2-whcyy'},
                    {id:'GY_SL_DCWCY_456',name:'研发中心4#/5#/6#',imgName: 'W2-whcyy'},
                    {id:'GY_SL_DCWCY_LD',name:'展示中心',imgName: 'W2-whcyy'},
                    {id:'GY_SL_DCWCY_YFZX',name:'研发会议中心1#',imgName: 'W2-whcyy'},
                    {id:'GY_SL_DCWCY_YYZX',name:'研发会议中心2#',imgName: 'W2-whcyy'}
                ],
                flag:true
            },{
                name:'砂之船奥特莱斯',
                arr:[
                    {id:'GY_SL_LB03_1',name:'1#',imgName: 'W2-szc'},
                    {id:'GY_SL_LB03_2',name:'2#',imgName: 'W2-szc'},
                    {id:'GY_SL_LB03_3',name:'3#',imgName: 'W2-szc'},
                    {id:'GY_SL_LB03_4',name:'4#',imgName: 'W2-szc'}
                ]
            },{
                name:'双龙数据工场',
                arr:[
                    {id:'GY_SL_LA04',name:'主楼',imgName: 'W2-sjgc'}
                ],
                flag:true
            },{
                name:' 中国文化大数据产业大楼',
                arr:[
                    {id:'GY_SL_LC02',name:'CCDI',imgName: 'W2-ccdi'}
                ],
                flag:true
            }]
        },
        {
            name:'企事业单位',
            arr:[{
                name:'贵阳市委党校',
                arr:[
                    {id:'GY_SL_LA09_1',name:'党校主楼',imgName: 'W2-swdx'},
                    {id:'GY_SL_LA09_4',name:'党校体育馆',imgName: 'W2-swdx'},
                    {id:'GY_SL_LA09_3',name:'党校食堂',imgName: 'W2-swdx'},
                    {id:'GY_SL_LA09_2',name:'党校宿舍',imgName: 'W2-swdx'},
                ]
            },{
                name:'贵阳学院',
                arr:[
                    {id:'GY_SL_LA03_1',name:'音乐学院',imgName: 'W2-gyxy'},
                    {id:'GY_SL_LA03_2',name:'博雅楼',imgName: 'W2-gyxy'}
                ]
            },{
                name:'',
                arr:[
                    {id:'GY_SL_LA01',name:'贵阳神奇学院',imgName: 'W2-sqxy'},
                    {id:'GY_SL_LA02',name:'武警医院',imgName: 'W2-wjyy'},
                    {id:'GY_SL_LB01',name:'老干妈',imgName: 'W2-lgm'}
                ],
                flag:true
            },]
        },
        {
            name:'景观与桥梁',
            arr:[{
                name:'山体景观',
                arr:[
                    {id:'GY_SL_LA13',name:'东北山区域',imgName: 'W2-jgyql'},
                    {id:'GY_SL_LA14',name:'东南山区域',imgName: 'W2-jgyql'},
                    {id:'GY_SL_LA11',name:'北山区域',imgName: 'W2-jgyql'},
                    {id:'GY_SL_LA12',name:'南山区域',imgName: 'W2-jgyql'},
                ],
                flag:true
            },{
                name:'桥梁景观',
                arr:[
                    {id:'GY_SL_LA16',name:'龙水路桥',imgName: 'W2-jgyql'},
                    {id:'GY_SL_LA17',name:'龙腾路桥',imgName: 'W2-jgyql'},
                    {id:'GY_SL_LA15',name:'兴业路桥',imgName: 'W2-jgyql'},
                ],
                flag:true
            },]
        },
        {
            name:'高层住宅',
            arr:[{
                name:'经典天成住宅小区',
                arr:[
                    {id:'GY_SL_LB02_1',name:'1#大楼',imgName: 'W2-jdtc'},
                    {id:'GY_SL_LB02_2',name:'12#大楼',imgName: 'W2-jdtc'}
                ]
            },{
                name:'庭宜小区住宅',
                arr:[
                    {id:'GY_SL_LA06_1',name:'1#大楼',imgName: 'W2-tyxq'},
                    {id:'GY_SL_LA06_2',name:'2#大楼',imgName: 'W2-tyxq'},
                    {id:'GY_SL_LA06_3',name:'3#大楼',imgName: 'W2-tyxq'},
                    {id:'GY_SL_LA06_4',name:'4#大楼',imgName: 'W2-tyxq'},
                    {id:'GY_SL_LA06_5',name:'5#大楼',imgName: 'W2-tyxq'},
                    {id:'GY_SL_LA06_6',name:'6#大楼',imgName: 'W2-tyxq'},
                ]
            },{
                name:'兴业小区住宅',
                arr:[
                    {id:'GY_SL_LA07_1',name:'1#大楼',imgName: 'W2-xyxq'},
                    {id:'GY_SL_LA07_2',name:'2#大楼',imgName: 'W2-xyxq'},
                    {id:'GY_SL_LA07_3',name:'3#大楼',imgName: 'W2-xyxq'},
                    {id:'GY_SL_LA07_4',name:'4#大楼',imgName: 'W2-xyxq'},
                ]
            },{
                name:'林园小区住宅',
                arr:[
                    {id:'GY_SL_LA05_1',name:'1#大楼',imgName: 'W2-lyxq'},
                    {id:'GY_SL_LA09_2',name:'1#大楼',imgName: 'W2-lyxq'},
                    {id:'GY_SL_LA09_3',name:'1#大楼',imgName: 'W2-lyxq'},
                    {id:'',name:''},
                ]
            }]
        }
    ]
}

function selectedCategoryFunction(index){
    selectedCategory=category[index]
    showLeftTable(selectedCategory)
    $('.network-btn').removeClass('network-btn-selected')
    $('.network-btns').find('.network-btn').eq(index).addClass('network-btn-selected')
}

function selectedItem(event,id){
    var target = null
    var nodeList=null
    var monomerIds=[]
    if(event && !id){
        var target = event.target
        var id=''
        var nodeList=null
        var monomerIds=[]
        if(target.tagName.toUpperCase() =='SPAN' && target.dataset.id){
            id=target.dataset.id
            $('.network-item-complete').removeClass('network-item-selected')
            $('.network-item-half').removeClass('network-item-selected')
            target.className+=' network-item-selected'
            selectedGroupName=target.dataset.parent
            selectedMonomerName=target.textContent
            selectedPictureName=target.dataset.img
            nodeList=target.parentNode.parentElement.querySelectorAll('span')
            for(var i=0;i<nodeList.length;i++){
                monomerIds.push({
                    id:nodeList[i].dataset.id,
                    name:nodeList[i].textContent
                })
            }
        }else{
            return
        }
    }else{
        nodeList=$('#categoryTable').find('.network-table').eq(0).find('span')
        for(var i=0;i<nodeList.length;i++){
            monomerIds.push({
                id:nodeList[i].dataset.id,
                name:nodeList[i].textContent
            })
        }
        $('#categoryTable').find('.network-table').eq(0).find('span').eq(0).addClass('network-item-selected')
    }
    for(var i=0;i<equipState.length;i++){
        if(equipState[i].id == id){
            selectedEquip = equipState[i]
            break
        }
    }
    for(var j=0;j<loopState.length;j++){
        if(loopState[j].id == id){
            selectedLoop = loopState[j]
            break
        }
    }

    // 获取该群下所有单体的网络延迟
    var monomerStates=[]
    for(var i=0;i<monomerIds.length;i++){
        for(var j=0;j<equipState.length;j++){
           if(monomerIds[i].id == equipState[j].id){
               monomerStates.push({
                   id:monomerIds[i].id,
                   name:monomerIds[i].name,
                   delay:equipState[j].delay
               })
               break
           }
        }
    }
    // 回路组值与状态处理
    var loopData={
        DO1:{sum:0,online:0},
        DO2:{sum:0,online:0},
        DO3:{sum:0,online:0},
        DO4:{sum:0,online:0},
    }
    Object.keys(selectedLoop).forEach(function(item,index){
        var arr=item.split('_')
        if(loopData[arr[0]]){
            loopData[arr[0]].sum++
            if(selectedLoop[item].value === true){
                loopData[arr[0]].online++
            }
        }
    })
    var sumData=[]
    var onlineData=[]
    Object.keys(loopData).forEach(function(item,index){
        if(loopData[item].sum>0){
            sumData.push(loopData[item].sum)
            onlineData.push(loopData[item].online)
        }
    })
    showBar(sumData,onlineData)
    showPie(monomerStates)
    showEquipChart(selectedEquip)
    showChartTitle()
    showPicture()
}

// 显示左边表格
function showLeftTable(selectedCategory){
    var html=''
    if(selectedCategory.arr.length>0){
        for(var i=0;i<selectedCategory.arr.length;i++){
            html+='<ul class="network-table" onclick="selectedItem(event)">'
            if(selectedCategory.arr[i].name){
                html+='<li class="network-table-title">'
                html+=selectedCategory.arr[i].name
                html+='</li>'
            }

            if(selectedCategory.arr[i].flag){
                for(var j=0;j<selectedCategory.arr[i].arr.length;j++){
                    html+='<li class="network-table-item">'
                    html+='<span class="network-item-complete" data-id="'+selectedCategory.arr[i].arr[j].id+'" data-parent="'+selectedCategory.arr[i].name+'" data-img="'+selectedCategory.arr[i].arr[j].imgName+'">'+selectedCategory.arr[i].arr[j].name+'</span>'
                    html+='</li>'
                }
            }else{
                for(var j=0;j<selectedCategory.arr[i].arr.length;j=j+2){
                    html+='<li class="network-table-item">'
                    html+='<span class="network-item-half" data-id="'+selectedCategory.arr[i].arr[j].id+'" data-parent="'+selectedCategory.arr[i].name+'" data-img="'+selectedCategory.arr[i].arr[j].imgName+'">'+selectedCategory.arr[i].arr[j].name+'</span>'
                    html+='<span class="network-item-half" data-id="'+selectedCategory.arr[i].arr[j+1].id+'" data-parent="'+selectedCategory.arr[i].name+'" data-img="'+selectedCategory.arr[i].arr[j].imgName+'">'+selectedCategory.arr[i].arr[j+1].name+'</span>'
                    html+='</li>'
                }
            }
            html+='</ul>'

        }
        $('#categoryTable').html(html)
        //默认第一个选中
        selectedGroupName=selectedCategory.arr[0].name
        selectedMonomerName=selectedCategory.arr[0].arr[0].name
        selectedPictureName=selectedCategory.arr[0].arr[0].imgName
        selectedItem(null,selectedCategory.arr[0].arr[0].id)
    }
}

// 显示柱形图
function showBar(sumData,onlineData){
    var myChart = echarts.init(document.getElementById('networkBarChart'));
    option = {
        tooltip : {
            trigger: 'axis'
        },
        calculable : true,
        grid: {y: 10, y2:30, x2:0},
        xAxis : [
            {
                type : 'category',
                data : ['G1','G2','G3','G4'],
                axisLabel:{
                    formatter:'{value}',
                    textStyle: {
                        color: '#ffffff',
                        fontSize: 16
                    }
                },
                axisLine:{
                    lineStyle:{
                        color:'#ffffff'
                    }
                }
            },
            {
                type : 'category',
                axisLine: {show:false},
                axisTick: {show:false},
                splitArea: {show:false},
                splitLine: {show:false},
                axisLabel: {show:false},
                data : ['G1','G2','G3','G4']
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel:{
                    formatter:'{value}',
                    textStyle: {
                        color: '#ffffff',
                        fontSize: 16
                    }
                },
                splitLine:{
                    show:true
                },
                axisLine:{
                    lineStyle:{
                        color:'#ffffff'
                    }
                }
            }
        ],
        series : [
            {
                name:'',
                type:'bar',
                barWidth:30,
                xAxisIndex:1,
                itemStyle: {normal: {color:'rgba(50,205,50,1)',barBorderRadius:[15,15,0,0], label:{show:false,formatter:function(p){return p.value > 0 ? (p.value +'\n'):'';}}}},
                data:onlineData
            },
            {
                name:'',
                type:'bar',
                barWidth:30,
                itemStyle: {opacity:0.5,normal: {color:'rgba(144,238,144,0.2)', barBorderRadius:[15,15,0,0],label:{show:false}}},
                data:sumData
            },

        ]
    };
    myChart.setOption(option);
}

// 显示回路等状态
function showEquipChart(equipState){
    var html=''
    if(equipState.router === 'true'){
        html+='<p class="network-equip-item network-equip-green">网络</p>'
    }else if(equipState.route === 'false'){
        html+='<p class="network-equip-item network-equip-red">网络</p>'
    }else{
        html+='<p class="network-equip-item network-equip-grey">网络</p>'
    }
    if(equipState.ecue === 'true'){
        html+='<p class="network-equip-item network-equip-green">回路</p>'
    }else if(equipState.ecue === 'false'){
        html+='<p class="network-equip-item network-equip-red">回路</p>'
    }else{
        html+='<p class="network-equip-item network-equip-grey">回路</p>'
    }
    if(equipState.corej === 'true'){
        html+='<p class="network-equip-item network-equip-green">动画</p>'
    }else if(equipState.corej === 'false'){
        html+='<p class="network-equip-item network-equip-red">动画</p>'
    }else{
        html+='<p class="network-equip-item network-equip-grey">动画</p>'
    }
    $('#equipState').html(html)
}

// 显示饼形图
function showPie(monomerStates){
    var myChart = echarts.init(document.getElementById('networkPieChart'));
    var dataStyle = {
        normal: {
            label: {show:false},
            labelLine: {show:false}
        }
    };
    var placeHolderStyle = {
        normal : {
            color: 'rgba(0,0,0,0)',
            label: {show:false},
            labelLine: {show:false}
        },
        emphasis : {
            color: 'rgba(0,0,0,0)'
        }
    };
    var optionValue=[]
    for(var i=0;i<monomerStates.length;i++){
        optionValue.push({
            name:monomerStates[i].name,
            type:'pie',
            clockWise:true,
            radius : [100-15*i, 112-15*i],
            itemStyle : dataStyle,
            data:[
                {
                    value:monomerStates[i].delay,
                    name:'网络延迟'
                },
                {
                    value:100-Number(monomerStates[i].delay),
                    name:'',
                    show:false
                }
            ]
        })
    }

    option = {
        tooltip : {
            show: true,
            formatter: "{a} <br/>{b} : {c}"
        },
        series : optionValue
    };
    myChart.setOption(option)
}

// 显示表格题目
function showChartTitle(){
    $('#networkBarTitle').html(selectedGroupName+'&nbsp;'+selectedMonomerName+'网络和设备状态')
    $('#networkPieTitle').html(selectedGroupName+'网络延迟')
}

// 显示图片
function showPicture() {
    var path=location.href.substring(0,location.href.lastIndexOf('/'))
    $("#networkPicture").attr('src',path+'/common/img/W2/'+selectedPictureName+'.png')
}